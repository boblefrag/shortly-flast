import settings
import views
from urls import url_map
from flast.app import RedisApp, BaseApp, create_app


class App(RedisApp, BaseApp):
    def __init__(self):
        self.url_map = url_map
        self.views = views
        super(App, self).__init__()

shortly = create_app(App, with_static=settings.DEBUG)

if __name__ == '__main__':
    from werkzeug.serving import run_simple
    run_simple('0.0.0.0', 5555, shortly, use_debugger=True, use_reloader=True)


