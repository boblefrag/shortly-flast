from werkzeug.routing import Map, Rule

url_map = Map([
    Rule('/', endpoint='new'),
    Rule('/<short_id>', endpoint='follow'),
    Rule('/<short_id>+', endpoint='details')
])
