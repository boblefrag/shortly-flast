# create you views here. example:
import urlparse
from flast.views import View
from app import shortly as app
from werkzeug.utils import redirect
from werkzeug.exceptions import NotFound

class New(View):
    def get(self, request, error=None, url=None):
        return self.render_template('new_url.html', error=error, url=url)

    def is_valid_url(self, url):
        parts = urlparse.urlparse(url)
        return parts.scheme in ('http', 'https')

    def post(self, request):
        error = None
        url = request.form['url']
        if not self.is_valid_url(url):
            error = 'Please enter a valid URL'
        else:
            short_id = self.insert_url(url)
            return redirect('/%s+' % short_id)
        return self.render_template('new_url.html', error=error, url=url)

    def insert_url(self, url):
        short_id = app.redis.get('reverse-url:' + url)
        if short_id is not None:
            return short_id
        url_num = app.redis.incr('last-url-id')
        short_id = self.base36_encode(url_num)
        app.redis.set('url-target:' + short_id, url)
        app.redis.set('reverse-url:' + url, short_id)
        return short_id

    def base36_encode(self, number):
        assert number >= 0, 'positive integer required'
        if number == 0:
            return '0'
        base36 = []
        while number != 0:
            number, i = divmod(number, 36)
            base36.append('0123456789abcdefghijklmnopqrstuvwxyz'[i])
        return ''.join(reversed(base36))


class Follow(View):
    def get(self, request, short_id):
        link_target = app.redis.get('url-target:' + short_id)
        if link_target is None:
            raise NotFound()
        app.redis.incr('click-count:' + short_id)
        return redirect(link_target)


class Details(View):
    def get(self, request, short_id):

        link_target = app.redis.get('url-target:' + short_id)
        if link_target is None:
            raise NotFound()
        click_count = int(app.redis.get('click-count:' + short_id) or 0)
        return self.render_template('short_link_details.html',
                                    link_target=link_target,
                                    short_id=short_id,
                                    click_count=click_count
                                    )
